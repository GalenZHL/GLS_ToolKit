

Pod::Spec.new do |s|

  s.name         = "GLS_ToolKit"
  s.version      = "0.0.1"
  s.summary      = "工具模块"

  s.homepage     = "https://gitee.com/GalenZHL/GLS_ToolKit"
  s.license      = { :type => "MIT", :file => "README.md" }
  s.author             = { "Galen" => "18668088548@163.com" }

  s.platform     = :ios
  s.ios.deployment_target = "9.0"


  s.source       = { :git => "https://gitee.com/GalenZHL/GLS_ToolKit.git", :tag => s.version }

  s.source_files  = "Classes", "GLS_ToolKit/Classes/**/*"
  s.resources = "GLS_ToolKit/Resources/**/*"
  s.requires_arc = true
  # s.libraries = "iconv", "xml2"

  #s.dependency "JSONKit", "~> 1.4"
  s.dependency "Masonry"
  s.dependency "ReactiveCocoa"
  s.dependency "SDWebImage"
end
