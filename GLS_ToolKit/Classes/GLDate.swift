//
//  GLDate.swift
//  GLS_ToolKit
//
//  Created by zhl on 2018/10/26.
//
import UIKit
//MARK: - 时间节点
//未来某天的时间点
public func dateWithDaysFromNow(_ days:integer_t) -> NSDate {
    return dateByAddingDays(days)
}
//过去某天的时间点
public func dateWithDaysBeforeNow(_ days:integer_t) -> NSDate {
    return dateBySubtractingDays(days)
}
//明天
public func dateTomorrow() -> NSDate {
    return dateWithDaysFromNow(1)
}
//昨天
public func dateYesterday() -> NSDate {
    return dateWithDaysBeforeNow(1)
}
//距离现在多少个小时的未来某个时间点
public func dateWithHoursFromNow(_ hours: integer_t) -> NSDate {
    let atimeInterval:TimeInterval = Double(NSDate.init().timeIntervalSinceReferenceDate) + Double(3600 * hours)
    let newDate = NSDate.init(timeIntervalSinceReferenceDate: atimeInterval)
    return newDate
}
//距离现在多少个小时的过去某个时间点
public func dateWithHoursBeforeNow(_ hours: integer_t) -> NSDate {
    let atimeInterval:TimeInterval = Double(NSDate.init().timeIntervalSinceReferenceDate) - Double(3600 * hours)
    let newDate = NSDate.init(timeIntervalSinceReferenceDate: atimeInterval)
    return newDate
}
//距离现在多少分钟的未来某个时间点
public func dateWithMinutesFromNow(_ minutes: integer_t) -> NSDate {
    let atimeInterval:TimeInterval = Double(NSDate.init().timeIntervalSinceReferenceDate) + Double(60 * minutes)
    let newDate = NSDate.init(timeIntervalSinceReferenceDate: atimeInterval)
    return newDate
}
//距离现在多少分钟的过去某个时间点
public func dateWithMinutesBeforeNow(_ minutes: integer_t) -> NSDate {
    let atimeInterval:TimeInterval = Double(NSDate.init().timeIntervalSinceReferenceDate) - Double(60 * minutes)
    let newDate = NSDate.init(timeIntervalSinceReferenceDate: atimeInterval)
    return newDate
}

//MARK: - 日期拆分
//小时
public func DateHour(_ date:NSDate) -> Int {
    let components = currentCalendar().components(calendarUnit(), from: date as Date)
    return components.hour ?? 0
}
//分钟
public func DateMinute(_ date:NSDate) -> Int {
    let components = currentCalendar().components(calendarUnit(), from: date as Date)
    return components.minute ?? 0
}
//秒钟
public func DateSecond(_ date:NSDate) -> Int {
    let components = currentCalendar().components(calendarUnit(), from: date as Date)
    return components.second ?? 0
}
//年
public func DateYear(_ date:NSDate) -> Int {
    let components = currentCalendar().components(calendarUnit(), from: date as Date)
    return components.year ?? 0
}
//月
public func DateMonth(_ date:NSDate) -> Int {
    let components = currentCalendar().components(calendarUnit(), from: date as Date)
    return components.month ?? 0
}
//日
public func DateDay(_ date:NSDate) -> Int {
    let components = currentCalendar().components(calendarUnit(), from: date as Date)
    return components.day ?? 0
}
//一周的第几天， 从周日开始
public func DateWeekDay(_ date:NSDate) -> Int {
    let components = currentCalendar().components(calendarUnit(), from: date as Date)
    return components.weekday ?? 0
}
//每月的第几周
public func DatenthWeekday(_ date:NSDate) -> Int {
    let components = currentCalendar().components(calendarUnit(), from: date as Date)
    return components.weekdayOrdinal ?? 0
}

//MARK: - 日期判断
//判断两个日期是不是同一天
public func isEqualToDateIgnoringTime(_ dateOne:NSDate, dateTwo:NSDate) -> Bool {
    let components1 = currentCalendar().components(calendarUnit(), from: dateOne as Date)
    let components2 = currentCalendar().components(calendarUnit(), from: dateTwo as Date)
    return ((components1.year == components2.year) && (components1.month == components2.month) && (components1.day == components2.day))
}

//判断是不是今天
public func isToday(_ date:NSDate) -> Bool {
    return isEqualToDateIgnoringTime(date, dateTwo:NSDate.init())
}
//判断是不是明天
public func isTomorrow(_ date:NSDate) -> Bool {
    return isEqualToDateIgnoringTime(date, dateTwo: dateTomorrow())
}
//判断是不是明天
public func isYesterday(_ date:NSDate) -> Bool {
    return isEqualToDateIgnoringTime(date, dateTwo: dateYesterday())
}
//判断是不是同一周
public func isSameWeekAsDate(_ dateOne:NSDate, dateTwo:NSDate) -> Bool {
    let components1 = currentCalendar().components(calendarUnit(), from: dateOne as Date)
    let components2 = currentCalendar().components(calendarUnit(), from: dateTwo as Date)
    if components1.weekOfMonth != components2.weekOfMonth {
        return false
    }
    return fabs(dateOne.timeIntervalSince(dateTwo as Date)) < 604800.0
}
//判断是不是本周
public func isThisWeek(_ date:NSDate) -> Bool {
    return isSameWeekAsDate(date, dateTwo: NSDate.init())
}
//判断是不是下周
public func isNextWeek(_ date:NSDate) -> Bool {
    let aTimeInterval = NSDate.init().timeIntervalSinceReferenceDate + 604800.0
    let newDate = NSDate.init(timeIntervalSinceReferenceDate: aTimeInterval)
    return isSameWeekAsDate(date, dateTwo: newDate)
}
//判断是不是上周
public func isLastWeek(_ date:NSDate) -> Bool {
    let aTimeInterval = NSDate.init().timeIntervalSinceReferenceDate - 604800.0
    let newDate = NSDate.init(timeIntervalSinceReferenceDate: aTimeInterval)
    return isSameWeekAsDate(date, dateTwo:newDate)
}
//判断是不是同一个月
public func isSameMonthAsDate(_ dateOne:NSDate, dateTwo:NSDate) -> Bool {
    let components1 = currentCalendar().components(NSCalendar.Unit.init(rawValue: NSCalendar.Unit.year.rawValue | NSCalendar.Unit.month.rawValue), from:dateOne as Date)
    let components2 = currentCalendar().components(NSCalendar.Unit.init(rawValue: NSCalendar.Unit.year.rawValue | NSCalendar.Unit.month.rawValue), from: dateTwo as Date)
    return ((components1.month == components2.month) && (components1.year == components2.year))
}
//判断是不是这个月
public func isThisMonth(_ date:NSDate) -> Bool {
    return isSameMonthAsDate(date, dateTwo: NSDate.init())
}
//判断是不是上个月
public func isLastMonth(_ date:NSDate) -> Bool {
    return isSameMonthAsDate(date, dateTwo:dateBySubtractingMonth(1, date: NSDate.init()))
}
//判断是不是下个月
public func isNextMonth(_ date:NSDate) -> Bool {
    return isSameMonthAsDate(date, dateTwo:dateByAddingMonth(1, date: NSDate.init()))
}
//判断是不是上一年
public func isSameYearAsDate(_ dateOne:NSDate,dateTwo:NSDate) -> Bool {
    let components1 = currentCalendar().components(NSCalendar.Unit.init(rawValue: NSCalendar.Unit.year.rawValue), from:dateOne as Date)
    let components2 = currentCalendar().components(NSCalendar.Unit.init(rawValue: NSCalendar.Unit.year.rawValue), from: dateTwo as Date)
    return (components1.year == components2.year)
}
//判断是不是今年
public func isThisYear(_ date:NSDate) -> Bool {
    return isSameYearAsDate(date, dateTwo:NSDate.init())
}
//判断是不是下一年
public func isNextYear(_ date:NSDate) -> Bool {
    return isSameYearAsDate(date, dateTwo:dateByAddingYears(1, date: NSDate.init()))
}
//判断是不是上一年
public func isLastYear(_ date:NSDate) -> Bool {
    return isSameYearAsDate(date, dateTwo:dateBySubtractingYears(1, date: NSDate.init()))
}

//MARK: - 私有方法
//过去多少天的某个时间点
public func dateBySubtractingDays(_ days:integer_t) -> NSDate {
    return dateByAddingDays(days * (-1))
}
//未来多少天的某个时间点
public func dateByAddingDays(_ days:integer_t) -> NSDate {
    let dateComponents: DateComponents = DateComponents.init()
    let newDate: NSDate = NSCalendar.current.date(byAdding: dateComponents, to: NSDate.init() as Date, wrappingComponents: true)! as NSDate
    return newDate
}
//当前日期
public func currentCalendar() -> NSCalendar {
    let sharedCalendar : NSCalendar = NSCalendar.autoupdatingCurrent as NSCalendar
    return sharedCalendar;
}
//想要获取的某个时间的枚举
public func calendarUnit() -> NSCalendar.Unit {
    return NSCalendar.Unit(rawValue: NSCalendar.Unit.year.rawValue | NSCalendar.Unit.month.rawValue | NSCalendar.Unit.year.rawValue | NSCalendar.Unit.day.rawValue | NSCalendar.Unit.hour.rawValue | NSCalendar.Unit.minute.rawValue | NSCalendar.Unit.second.rawValue | NSCalendar.Unit.weekday.rawValue | NSCalendar.Unit.weekdayOrdinal.rawValue)
}
//未来多少年的某个时间点
public func dateByAddingYears(_ dYears:Int, date:NSDate) -> NSDate {
    let dateComponents = NSDateComponents.init()
    dateComponents.year = dYears
    let newDate: NSDate = currentCalendar().date(byAdding: dateComponents as DateComponents, to: date as Date, options: NSCalendar.Options(rawValue: 0))! as NSDate
    return newDate
}
//过去多少年的某个时间点
public func dateBySubtractingYears(_ dYears:Int, date:NSDate) -> NSDate {
    return dateByAddingYears(-dYears, date: date)
}
//未来多少月的某个时间点
public func dateByAddingMonth(_ dMonth:Int, date:NSDate) -> NSDate {
    let dateComponents = NSDateComponents.init()
    dateComponents.month = dMonth
    let newDate: NSDate = currentCalendar().date(byAdding: dateComponents as DateComponents, to: date as Date, options: NSCalendar.Options(rawValue: 0))! as NSDate
    return newDate
}
//过去多少年的某个时间点
public func dateBySubtractingMonth(_ dMonth:Int, date:NSDate) -> NSDate {
    return dateByAddingMonth(-dMonth, date: date)
}
