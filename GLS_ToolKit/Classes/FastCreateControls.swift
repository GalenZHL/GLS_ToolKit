
//
//  FastCreateControls.swift
//  GLS_ToolKit
//
//  Created by zhl on 2018/10/26.
//
import UIKit
//MARK: - label
public func createLabel(text:String, textColor:UIColor, font:CGFloat, textAlignment:NSTextAlignment) -> UILabel {
    let label = UILabel.init()
    label.text = text;
    label.textColor = textColor;
    label.font = UIFont.systemFont(ofSize: font)
    label.textAlignment = textAlignment
    return label
}


//MARK: - button
public func createButtonTitle(title:String, titleColor:UIColor, font:CGFloat, backgroundColor:UIColor) -> UIButton {
    let button:UIButton = UIButton.init()
    button.setTitle(title, for: .normal)
    button.setTitleColor(titleColor, for: .normal)
    button.titleLabel?.font = UIFont.systemFont(ofSize: font)
    button.backgroundColor = backgroundColor
    return button
}

public func createButtonImage(image:UIImage, title:String, titleColor:UIColor, font:CGFloat, backgroundColor:UIColor) -> UIButton {
    let button: UIButton = createButtonTitle(title: title, titleColor: titleColor, font: font, backgroundColor: backgroundColor)
    button.setImage(image, for: .normal)
    return button
}

