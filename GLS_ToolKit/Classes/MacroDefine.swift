//
//  MacroDefine.swift
//  GLS_ToolKit
//
//  Created by zhl on 2018/10/26.
//

import UIKit

///*! 导航栏的高 44 */
 public let GLNavigationBar_h:CGFloat = 44
///*! 状态栏的高  */
public let GLStatusBar_h:CGFloat = UIApplication.shared.statusBarFrame.height
///*! tabBar 的高 49 */
public var GLTabBar_h = GLStatusBar_h > 20.0 ? 83.0 : 49.0
///** 动态获取状态栏+导航栏高度**/
public let GLNavigation_Status_h = GLNavigationBar_h + GLStatusBar_h
////安全区高度
public var GLSafeAreaHeight = GLStatusBar_h > 20 ? 34 : 0;
////获取屏幕 宽度、高度
public let SCREEN_WIDTH = UIScreen.main.bounds.size.width;
public let SCREEN_HEIGHT = UIScreen.main.bounds.size.height;
public let SCREEN_BOUNDS = UIScreen.main.bounds;

//获取系统版本
public let IOS_VERSION: Float = Float(UIDevice.current.systemVersion)!
public let currentSystemVersion = UIDevice.current.systemVersion

func LoadImage(_ imagePath:String) -> UIImage {
    return (UIImage.init(named:"haha") ?? nil)!
}

//----------------------颜色类---------------------------
// rgb颜色转换（16进制->10进制）
func UIColorFromRGB(rgbValue:String) -> UIColor {
    var cstr = rgbValue.trimmingCharacters(in:  CharacterSet.whitespacesAndNewlines).uppercased() as NSString;
    if(cstr.length < 6){
        return UIColor.clear;
    }
    if(cstr.hasPrefix("0X")){
        cstr = cstr.substring(from: 2) as NSString
    }
    if(cstr.hasPrefix("#")){
        cstr = cstr.substring(from: 1) as NSString
    }
    if(cstr.length != 6){
        return UIColor.clear;
    }
    var range = NSRange.init()
    range.location = 0
    range.length = 2
    //r
    let rStr = cstr.substring(with: range);
    //g
    range.location = 2;
    let gStr = cstr.substring(with: range)
    //b
    range.location = 4;
    let bStr = cstr.substring(with: range)
    var r :UInt32 = 0x0;
    var g :UInt32 = 0x0;
    var b :UInt32 = 0x0;
    Scanner.init(string: rStr).scanHexInt32(&r);
    Scanner.init(string: gStr).scanHexInt32(&g);
    Scanner.init(string: bStr).scanHexInt32(&b);
    return UIColor.init(red: CGFloat(r)/255.0, green: CGFloat(g)/255.0, blue: CGFloat(b)/255.0, alpha: 1);
}

// 获取RGB颜色
func RGBA(_ red:CGFloat, green:CGFloat, blue:CGFloat, a:CGFloat) -> UIColor {
    return UIColor.init(red: red, green: green, blue: blue, alpha: a)
}

func RGB(_ red:CGFloat, green:CGFloat, blue:CGFloat) -> UIColor {
    return RGBA(red, green: green, blue: blue, a: 1.0)
}


